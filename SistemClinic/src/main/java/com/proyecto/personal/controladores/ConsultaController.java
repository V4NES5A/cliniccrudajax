package com.proyecto.personal.controladores;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.proyecto.personal.entidades.Consulta;
import com.proyecto.personal.entidades.DetalleConsulta;
import com.proyecto.personal.entidades.Doctor;
import com.proyecto.personal.entidades.Paciente;
import com.proyecto.personal.repositorios.IPacienteRepository;
import com.proyecto.personal.service.ConsultaService;
import com.proyecto.personal.service.DoctorService;

@Controller
@CrossOrigin
@RequestMapping("consultas")
public class ConsultaController {

	// Repositorio para el manejo de datos
	@Autowired
	ConsultaService consultaService;
	@Autowired
	DoctorService doctorService;	
	@Autowired
	IPacienteRepository pacienteRepository;  

	@GetMapping(value="index")
    public String index() {
        return new String("/views/consulta/consulta");
    }
	
	//mantener los datos de detalles consulta de forma staticos 
	public static List<DetalleConsulta> detalles=new ArrayList<>();
	
	public ConsultaController() {
        detalles=new ArrayList<>();
    }
	
		
	// metodo para traer los doctores
		@GetMapping(value = "getDoctores", produces = MediaType.APPLICATION_JSON_VALUE)
		@ResponseBody
		@CrossOrigin
		public Object getDoctores() {
			
			//Se crea una lista para asignar un seudonimo
			List<HashMap<String,Object>> registros=new ArrayList<>();
			
			//Se crea una lista para almacenar los datos
			List<Doctor> doc=doctorService.getAll();
			
			//Se genera un for para recorrer la lista
			for (Doctor doctor : doc) {
				HashMap<String,Object> object = new HashMap<>();//Se crea un nuevo objeto hashmap
				object.put("id",doctor.getId());
				object.put("nombre", doctor.getNombre().toString());
				object.put("direccion", doctor.getDireccion().toString());
				object.put("especialidad", doctor.getEspecialidad().getEspecialidad());
				object.put("operaciones","<button class='btn btn-primary ml-2 agregarDoctor' data-dismiss='modal'>Agregar</button>");
				
				registros.add(object);
			}
			return Collections.singletonMap("data", registros);
		}
		// metodo para traer los pacientes
	 @GetMapping(value="getPacientes", produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseBody
	    @CrossOrigin
	    public Object getPacientes( ) {
		//Se crea una lista para asignar un seudonimo
	        List<HashMap<String,Object>> registros=new ArrayList<>();
	      //Se crea una lista para almacenar los datos
	        List<Paciente> l=(List<Paciente>) pacienteRepository.findAll();

	        for (Paciente paciente : l) {
	            HashMap<String,Object> object=new HashMap<>();//Se crea un nuevo objeto hashmap
	            
	            object.put("id", paciente.getId());
	            object.put("nombre", paciente.getNombre());
	            object.put("direccion", paciente.getDireccion());
	            object.put("operaciones","<button type='button' class='btn btn-primary agregarPaciente'  data-dismiss='modal'>Agregar</button>");
	               
	            registros.add(object);
	        }
	        return Collections.singletonMap("data", registros);
	    }
	
	
	 @PostMapping(value="agregarDetalle", produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseBody
	    @CrossOrigin
	    public Object  agregaDetalle(@RequestParam String sintoma) {
	       //creando objeto de detalle y agregandole la data recibida en la peticion
	        DetalleConsulta entity=new DetalleConsulta();
	        entity.setSintoma(sintoma);

	        //agregando el onjeto detalle a las lista
	        detalles.add(entity);

	        HashMap<String,String> json= new HashMap<String,String>();
	        json.put("mensaje", "Detalle agregado correctamente");
	        json.put("status", "200");

	        return  json;
	    }
	
	@GetMapping(value="detalles", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @CrossOrigin
    public Object getDetalles() {
        return detalles;
    }

    @PostMapping(value="resetDetalles", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @CrossOrigin
    public Object resetDetalles() {
        detalles=new ArrayList<>();
        return "lista reseteada";
    }
	
	
	//listar registro de consulta
	@GetMapping(value = "all", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public List<Consulta> getAllConsulta() {
		return (List<Consulta>) consultaService.getAll();
	}
	
	
	//listar registros buscar por id, metodo getConsulta que se ocupa en la funcion preModificar
	     @GetMapping(value="getConsulta/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	     @ResponseBody
	     @CrossOrigin
	     public Consulta getConsulta(@PathVariable Integer id) { 
	         return  consultaService.getConsulta(id);
	     }
	     
	     //metodo para guardar registros
	@PostMapping(value = "guardar")
	@ResponseBody
	@CrossOrigin
	public HashMap<String, String> save(
			@RequestParam  @DateTimeFormat(pattern = "yyyy-MM-dd") Date fecha,
			@RequestParam String diagnostico, 
			@RequestParam Integer idDoctor,
			@RequestParam Integer idPaciente) {
		
		HashMap<String, String> jsonReturn = new HashMap<>();
		
		Consulta consulta = new Consulta();// creando objeto de consulta
		
		// asignado datos al objeto de doctor
		consulta.setFecha(fecha);
		consulta.setDiagnostico(diagnostico);
		consulta.setDoctor(consultaService.getDoctor(idDoctor));
		consulta.setPaciente(consultaService.getPaciente(idPaciente));
		
		for (DetalleConsulta detallesConsulta : detalles) {
            detallesConsulta.setConsulta(consulta);
        }        
		 consulta.setDetallesConsultas(detalles);
        
		
		// manejando cualquier excepcion de error
		try {
			if(consultaService.saveOrUpdate(consulta)) {
				// guardando registro de doctor
				jsonReturn.put("estado", "OK");
				jsonReturn.put("mensaje", "Registro guardado");
				return jsonReturn;
			}else {
				jsonReturn.put("estado", "ERROR");
				jsonReturn.put("mensaje", "Registro no guardado");
				return jsonReturn;
			}

			
		} catch (Exception e) {
			jsonReturn.put("estado", "ERROR");
			jsonReturn.put("mensaje", "Registro no guardado" + e.getMessage());
			return jsonReturn;
		}

	}

	// Metodo para Eliminar registros
	@GetMapping(value = "delete/{id}")
	@ResponseBody
	@CrossOrigin
	public HashMap<String, String> delete(@PathVariable Integer id) {

		HashMap<String, String> jsonReturn = new HashMap<>();

		try {
			// buscando registro
			Consulta consulta = consultaService.getConsulta(id);
			// eliminando registro
			consultaService.delete(consulta);
			jsonReturn.put("estado", "OK");
			jsonReturn.put("mensaje", "Registro eliminado");
			return jsonReturn;

		} catch (Exception e) {
			jsonReturn.put("estado", "ERROR");
			jsonReturn.put("mensaje", "Registro no guardado" + e.getMessage());
			return jsonReturn;
		}
	}

	//Metodo para actualizar registros
	@GetMapping(value = "update/{id}")
	@ResponseBody
	@CrossOrigin
	public HashMap<String, String> update(
			@RequestParam Integer id, 
			@RequestParam Date fecha,
			@RequestParam String diagnostico, 
			@RequestParam Integer idDoctor, 
			@RequestParam Integer idPaciente) {
		Consulta consulta = new Consulta();// creando objeto de consulta
		HashMap<String, String> jsonReturn = new HashMap<>();

		// asignado datos al objeto de doctor
		consulta.setId(id);
		consulta.setFecha(fecha);
		consulta.setDiagnostico(diagnostico);
		consulta.setDoctor(consultaService.getDoctor(idDoctor));
		consulta.setPaciente(consultaService.getPaciente(idPaciente));

		// manejando cualquier excepcion de error
		try {
			consultaService.saveOrUpdate(consulta);// guardando registro de doctor
			jsonReturn.put("estado", "OK");
			jsonReturn.put("mensaje", "Registro actualizado");
			return jsonReturn;

		} catch (Exception e) {
			jsonReturn.put("estado", "ERROR");
			jsonReturn.put("mensaje", "Registro no actualizado" + e.getMessage());
			return jsonReturn;
		}

	}

}
