package com.proyecto.personal.entidades;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "especialidad")
public class Especialidad {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private String especialidad;
	
	 @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
	 private List<Doctor> doctor;
	
	//Constructores
	public Especialidad() {
		
	}

	public Especialidad(Integer id, String especialidad) {
		super();
		this.id = id;
		this.especialidad = especialidad;
	}

	//Metodos get and set
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
}
