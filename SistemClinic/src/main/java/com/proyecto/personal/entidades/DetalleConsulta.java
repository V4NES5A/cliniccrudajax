package com.proyecto.personal.entidades;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class DetalleConsulta {
	
	 @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Integer id;

	    private String sintoma;

	    @ManyToOne(fetch = FetchType.EAGER)
	    private Consulta consulta;

		public DetalleConsulta() {
		}

		public DetalleConsulta(Integer id, String sintoma, Consulta consulta) {
			super();
			this.id = id;
			this.sintoma = sintoma;
			this.consulta = consulta;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getSintoma() {
			return sintoma;
		}

		public void setSintoma(String sintoma) {
			this.sintoma = sintoma;
		}

		public Consulta getConsulta() {
			return consulta;
		}

		public void setConsulta(Consulta consulta) {
			this.consulta = consulta;
		}	    
}
