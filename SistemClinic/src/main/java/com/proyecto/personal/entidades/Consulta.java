package com.proyecto.personal.entidades;



import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "consulta")
public class Consulta {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NotNull
	private Date fecha;
	
	@NotBlank(message = "campo requerido")
	private String diagnostico;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Doctor doctor;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Paciente paciente;
	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "consulta",cascade = CascadeType.ALL)
    private List<DetalleConsulta> detallesConsultas;
	
	//Constructores
	public Consulta() {
		
	}

	public Consulta(Integer id, Date fecha, String diagnostico, Doctor doctor, Paciente paciente,
			List<DetalleConsulta> detallesConsultas) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.diagnostico = diagnostico;
		this.doctor = doctor;
		this.paciente = paciente;
		this.detallesConsultas = detallesConsultas;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public List<DetalleConsulta> getDetallesConsultas() {
		return detallesConsultas;
	}

	public void setDetallesConsultas(List<DetalleConsulta> detallesConsultas) {
		this.detallesConsultas = detallesConsultas;
	}	
}
