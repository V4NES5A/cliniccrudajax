package com.proyecto.personal.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyecto.personal.entidades.Doctor;
import com.proyecto.personal.entidades.Especialidad;
import com.proyecto.personal.repositorios.IDoctorRepository;
import com.proyecto.personal.repositorios.IEspecialidadRepository;


@Service
public class DoctorService {
	//metodo para sobre escribir datos 
	@Autowired
	IDoctorRepository rdoctor;
	//metodo para sobre escribir datos 
	@Autowired
	IEspecialidadRepository respecialidad;
	
	//metodo para listar
		@Transactional
	public List<Doctor> getAll(){
		return (List<Doctor>) rdoctor.findAll();
		
	}

	// metodo para listar especialidades
	@Transactional
	public List<Especialidad> getAllEspecialidad() {
		return (List<Especialidad>) respecialidad.findAll();

	}
				//metodo para guardar y modificar
				@Transactional
	public Boolean saveOrUpdate(Doctor entity) {
		try {
			rdoctor.save(entity);
			return true;
					
		}catch(Exception e) {
			return false;
		}

	}
				//metodo para eliminar
				@Transactional
	public Boolean delete(Doctor entity) {
		try {
			rdoctor.delete(entity);
			return true;
					
		}catch(Exception e) {
			return false;
		}

	}
				//metodo para obener los datos
				@Transactional
	public Especialidad getEspecialidad(Integer id) {
		return respecialidad.findById(id).get();
	}
			
				@Transactional
	public Doctor getDoctor(Integer id) {
		return rdoctor.findById(id).get();
	}

}
