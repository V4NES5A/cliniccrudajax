$(document).ready(inicio);
function inicio(){
	cargarDatos();
    cargarPacientes();
    cargarDoctores();

   $("#guardarConsulta").click(guardar);

   $("#eliminarConsulta").click(eliminar);

   $("#modificarConsulta").click(modificar);

   $("#btnCancelar").click(reset);
}
function reset() {
    $("#id").val(null);
    $("#fecha").val(null);
    $("#sintomas").val(null);
    $("#diagnostico").val(null);
    $("#doctor").val(null);
    $("#paciente").val(null);
    
    $("#id").val(null);
    $("#fecha2").val(null);
    $("#sintomas2").val(null);
    $("#diagnostico2").val(null);
    $("#doctor2").val(null);
    $("#paciente2").val(null);
    
}

//METODO PARA CARGAR DATOS CON UNA PETICION AJAX
//EN LA TABLA.
function cargarDatos() {
	// peticion AJAX al back-end
	$.ajax({
        url:"consultas/all",
        method:"Get",
        data:null,
        success:procesarDatosTabla,
        error:errorPeticion
    });
}

		//procesando datos de la peticion

function procesarDatosTabla(response) {
    $("#tdatos").html(""); //reseteando la tabla
    response.forEach(item => {
        $("#tdatos").append(""
        +"<tr>"
            +"<td>"
                +""+item.id
            +"</td>"
            +"<td>"
                +""+item.fecha
            +"</td>"
            +"<td>"
                +""+item.sintomas
			+"</td>"
			+"<td>"
                +""+item.diagnostico
            +"</td>"
            +"<td>"
                +""+item.doctor.nombre
			+"</td>"
			+"<td>"
                +""+item.paciente.nombre
            +"</td>"
            +"<td>"
                +"<button onclick='preModificar("+item.id+");' class='btn btn-warning ml-2'>Modificar</button>"
                +"<button onclick='preEliminar("+item.id+");' class='btn btn-danger ml-2'>Eliminar</button>"
            +"</td>"
        +"</tr>"
        +"");
    });    
}
function errorPeticion(response) {
    alert("Error al realizar la peticion: "+response);
    console.log("Error al realizar la peticion: "+response);
}

function cargarDoctores() {
    $.ajax({
        url:"http://localhost:8080/doctores/all",
        method:"Get",
        data:null,
        success: function(response){
            response.forEach(item => {
                $("#doctor").append(""
                    +"<option value='"+item.id+"'>"+item.nombre+"</option>"
                +"");

                $("#doctor2").append(""
                +"<option value='"+item.id+"'>"+item.nombre+"</option>"
            +"");
             });  
        },
        error:errorPeticion
    });
}
function cargarPacientes() {
    $.ajax({
        url:"http://localhost:8080/pacientes/all",
        method:"Get",
        data:null,
        success: function(response){
            response.forEach(item => {
                $("#paciente").append(""
                    +"<option value='"+item.id+"'>"+item.nombre+"</option>"
                +"");

                $("#paciente2").append(""
                +"<option value='"+item.id+"'>"+item.nombre+"</option>"
            +"");
             });  
        },
        error:errorPeticion
    });
}

function guardar() {
    $.ajax({
        url:"http://localhost:8080/consultas/save",
        method:"Get",
        data:{
            fecha:$("#fecha").val(),
			sintomas:$("#sintomas").val(),
			diagnostico:$("#diagnostico").val(),
			idDoctor:$("#doctor").val(),
			idPaciente:$("#paciente").val()
        },
        success:function(response){
            alert(response.mensaje);
            cargarDatos();
            reset();
        },
        error:errorPeticion

    });
}

function preEliminar(id) {
  $("#idConsulta").val(id);
  $("#modalEliminarConsulta").modal();
}

function eliminar() {
    var id=$("#idConsulta").val();
    $.ajax({
        url:"consultas/delete/"+id,
        method:"Get",
        data:null,
        success:function(response){
            alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion

    });
}

function preModificar(id) {
    $("#modalModificarConsulta").modal();
    // alert(id);
    $.ajax({
        url:"consultas/getConsulta/"+id,
        method:"Get",
        success:function(response){
            $("#id").val(response.id);
            $("#fecha2").val(response.fecha);
			$("#sintomas2").val(response.sintomas);
			$("#diagnostico2").val(response.diagnostico);
			$("#doctor2").val(response.doctor.id);
			$("#paciente2").val(response.paciente.id);
        },
        error:errorPeticion

    });
}

function modificar() {
    var id=$("#id").val();
    $.ajax({
        url:"consultas/update/"+id,
        method:"Get",
        data:{
            id:id,
            fecha:$("#fecha2").val(),
			sintomas:$("#sintomas2").val(),
			diagnostico:$("#diagnostico2").val(),
			idDoctor:$("#doctor2").val(),
			idPaciente:$("#paciente2").val()
        },
        success:function(response){
            alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion

    });
}
