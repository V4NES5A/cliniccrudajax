$(document).ready(inicio);
function inicio(){
	cargarDatos();

	$("#guardarPaciente").click(guardar);

   $("#eliminarPaciente").click(eliminar);

   $("#modificarPaciente").click(modificar);
   $("#btnCancelar").click(reset);
}
function reset() {
    $("#id").val(null);
    $("#nombre").val(null);
    $("#direccion").val(null);
    
    $("#id").val(null);
    $("#nombre2").val(null);
    $("#direccion2").val(null);
}

//METODO PARA CARGAR DATOS CON UNA PETICION AJAX
//EN LA TABLA.
//cargar datos
function cargarDatos() {
    //peticion ajax para soliciytar datos de docctores
    $.ajax({
        url:"pacientes/all",
        method:"Get",
        data:null,
        success:procesarDatosTabla,
        error:errorPeticion
    });
}
//procesando datos de la peticion
function procesarDatosTabla(response) {
    $("#tDatos").html(""); //reseteando la tabla
    response.forEach(item => {
        $("#tDatos").append(""
        +"<tr>"
            +"<td>"+""+item.id+"</td>"
            +"<td>"+""+item.nombre+"</td>"
            +"<td>"+""+item.direccion+"</td>"
            +"<td>"
                +"<button onclick='preModificar("+item.id+");' class='btn btn-warning ml-2'>Modificar</button>"
                +"<button onclick='preEliminar("+item.id+");' class='btn btn-danger ml-2'>Eliminar</button>"
            +"</td>"
        +"</tr>"
        +"");
    });
}
function errorPeticion(response) {
    alert("Error al realizar la peticion: "+response);
    console.log("Error al realizar la peticion: "+response);
}

function guardar() {
    $.ajax({
        url:"pacientes/save",
        method:"Get",
        data:{
            nombre:$("#nombre").val(),
            direccion:$("#direccion").val()
        },
        success:function(response){
            alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion

    });
}

function preEliminar(id) {
	$("#idPaciente").val(id);
	$("#modalEliminarPaciente").modal();
  }
  function eliminar() {
    var id=$("#idPaciente").val();
    $.ajax({
        url:"pacientes/delete/"+id,
        method:"Get",
        data:null,
        success:function(response){
            alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion

    });
}
function preModificar(id) {
    $("#modalModificarPaciente").modal();
    // alert(id);
    $.ajax({
        url:"pacientes/getPaciente/"+id,
        method:"Get",
        success:function(response){
            $("#id").val(response.id);
            $("#nombre2").val(response.nombre);
            $("#direccion2").val(response.direccion);
        },
        error:errorPeticion

    });
    
}
function modificar() {
    var id=$("#id").val();
    $.ajax({
        url:"pacientes/update/"+id,
        method:"Get",
        data:{
            id:id,
            nombre:$("#nombre2").val(),
            direccion:$("#direccion2").val()
        },
        success:function(response){
            alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion

    });
}