

//arreglo con los campos que se solicitan en la funcion 
//agregarDoctor y agregarPaciente
let paciente={id:0,nombre:""};
let doctor={id:0,nombre:""};

//implementacion de todas las funciones
$(document).ready(function() {
    console.log("inicio");
    cargarPacientes();
    cargarDoctores();
    cargarDetalles();

    $("#agregarDetalle").click(agregarDetalle);
    $("#guardar").click(guardarConsulta);


    $("body").on('click', '.agregarPaciente', function(){
        
        agregarPaciente($(this).parent().parent().children('td:eq(0)').text(),$(this).parent().parent().children('td:eq(1)').text());
    });

    $("body").on('click', '.agregarDoctor', function(){
        
        agregarDoctor($(this).parent().parent().children('td:eq(0)').text(),$(this).parent().parent().children('td:eq(1)').text());
    });

    resetDetalles();

});
// --------------------------------------------------- PACIENTES --------------------------------
//function para cargar la tabla de paciente
function cargarPacientes() {
    console.log("cargar pacientes");
    $("#tablePacientes").DataTable({
        
        "ajax":{
            "url":"http://localhost:8080/consultas/getPacientes",
            "method":"Get"
        },
        "columns":[
            {
                "data":"id",
                "width":"5%"
            },
            {
                "data":"nombre",
                "width":"30%"
            },
            {
                "data":"direccion",
                "width":"40%"
            },
            {
                "data":"operaciones",
                "width":"10%"
            }
        ],
        "scrollY":200,
        "language" : {
            "lengthMenu" : "Mostrar _MENU_ ",
            "zeroRecords" : "Datos no encontrados",
            "info" : "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty" : "Datos no encontrados",
            "infoFiltered" : "(Filtrados por _MAX_ total registros)",
            "search" : "Buscar:",
            "paginate" : {
                "first" : "Primero",
                "last" : "Anterior",
                "next" : "Siguiente",
                "previous" : "Anterior"
            }
        }
    });
}
//function para ver info de paciente en el input
function agregarPaciente(id,nombre) {     
    paciente.id=id;
    paciente.nombre=nombre;
    $("#paciente").val(nombre);
    console.log(paciente);
}

// ----------------------------------------- DOCTOR ----------------------------------
//function para cargar la tabla de doctor
function cargarDoctores() {
    console.log("cargar doctores");
    $("#tableDoctores").DataTable({
        
        "ajax":{
            "url":"http://localhost:8080/consultas/getDoctores",
            "method":"Get"
        },
        "columns":[
            {
                "data":"id"
            },
            {
                "data":"nombre"
            },
            {
                "data":"direccion"
            },
            {
                "data":"especialidad"
            },
            {
                "data":"operaciones"
            }
        ],
        "scrollY":200,
        "language" : {
            "lengthMenu" : "Mostrar _MENU_ ",
            "zeroRecords" : "Datos no encontrados",
            "info" : "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty" : "Datos no encontrados",
            "infoFiltered" : "(Filtrados por _MAX_ total registros)",
            "search" : "Buscar:",
            "paginate" : {
                "first" : "Primero",
                "last" : "Anterior",
                "next" : "Siguiente",
                "previous" : "Anterior"
            }
        }
    });
}
//function para ver info de doctor en el input
function agregarDoctor(id,nombre) {       
    doctor.id=id;
    doctor.nombre=nombre;
    $("#doctor").val(nombre);
    console.log(doctor);
}



// -----------------------------------DETALLES ------------------------------------
//function para agregar sintomas
function agregarDetalle() {
    console.log("agregar detalle");
    $.ajax({
        url:"http://localhost:8080/consultas/agregarDetalle",
        method:"Post",
        data:{
            sintoma:$("#sintoma").val()
        },
        success:function(response){
            console.log(response);
            cargarDetalles();
            $("#sintoma").val("");
        },
        error:errorPeticion
    });
}

function cargarDetalles() {
    console.log("cargar detalles");
    $.ajax({
        url:"http://localhost:8080/consultas/detalles",
        method:"Get",
        success:function(response){
            $("#tDetalles").html(""); //reseteando datos

            //cargando datos en la tabla
            response.forEach(i => {
                $("#tDetalles").append(""
                +"<tr>"
                    +"<td>"+i.sintoma+"</td>"
                    +"<td><button class='btn btn-danger'>eliminar</button></td>"
                +"</tr>");
            });
        },
        error:errorPeticion
    });
}
//function para los errores
function errorPeticion(response) {    
    console.log(response);
}

function resetDetalles() {
    console.log("reseteo de detalles");
    $.ajax({
        url:"http://localhost:8080/consultas/resetDetalles",
        method:"Post"

    });
}
// -------------------------------- CONSULTAS ----------------------------
function guardarConsulta() {
    $.ajax({
        url:"http://localhost:8080/consultas/guardar",
        method:"Post",
        data:{
            fecha:$("#fecha").val(),
            diagnostico:$("#diagnostico").val(),
            idDoctor:doctor.id,
            idPaciente:paciente.id
        },
        success:function(response){
            alert(response.mensaje);
            //location.reload();
        },
        error:errorPeticion
    });

}