$(document).ready(inicio);
function inicio(){

    //envocaciones de funciones
    cargarDatos();    

	$("#guardarEspecialidad").click(guardar);

   $("#eliminarEspecialidad").click(eliminar);

   $("#modificarEspecialidad").click(modificar);

   $("#btnCancelar").click(reset);
}
//funcion para limpear input
function reset() {
    $("#id").val(null);
    $("#especialidad").val(null);
    
    $("#id").val(null);
    $("#especialidad2").val(null);
}

//METODO PARA CARGAR DATOS CON UNA PETICION AJAX
//EN LA TABLA.
//cargar datos
function cargarDatos() {
    //peticion ajax para soliciytar datos de docctores
    $.ajax({
        url:"http://localhost:8080/especialidades/all",
        method:"Get",
        data:null,
        success:procesarDatosTabla,
        error:errorPeticion
    });
}
//procesando datos de la peticion
function procesarDatosTabla(response) {
    $("#tDatos").html(""); //reseteando la tabla
    response.forEach(item => {
        $("#tDatos").append(""
        +"<tr>"
            +"<td>"+""+item.id+"</td>"
            +"<td>"+""+item.especialidad+"</td>"
            +"<td>"
                +"<button onclick='preModificar("+item.id+");' class='btn btn-warning ml-2'>Modificar</button>"
                +"<button onclick='preEliminar("+item.id+");' class='btn btn-danger ml-2'>Eliminar</button>"
            +"</td>"
        +"</tr>"
        +"");
    });
}

function errorPeticion(response) {
    alert("Error al realizar la peticion: "+response);
    console.log("Error al realizar la peticion: "+response);
}
function guardar() {
    $.ajax({
        url:"http://localhost:8080/especialidades/save",
        method:"Get",
        data:{
            especialidad:$("#especialidad").val()
        },
        success:function(response){
            //alert(response.mensaje);
            SuccessAlert();
            cargarDatos();
        },
        error:errorPeticion

    });
}
//se carga el id en el modal #modalEliminarEspecialidad
function preEliminar(id) {
    $("#modalEliminarEspecialidad").modal();
	$("#idEspecialidad").val(id);
  }


  function eliminar() {
    var id=$("#idEspecialidad").val();
    $.ajax({
        url:"http://localhost:8080/especialidades/delete/"+id,
        method:"Get",
        data:null,
        success:function(response){
            alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion

    });
}
function preModificar(id) {
    $("#modalModificarEspecialidad").modal();
    // alert(id);
    $.ajax({
        url:"http://localhost:8080/especialidades/getEspecialidad/"+id,
        method:"Get",
        success:function(response){
            $("#id").val(response.id);
            $("#especialidad2").val(response.especialidad);
        },
        error:errorPeticion

    });
    
}
function modificar() {
    var id=$("#id").val();
    $.ajax({
        url:"http://localhost:8080/especialidades/update/"+id,
        method:"Get",
        data:{
            id:id,
            especialidad:$("#especialidad2").val()
        },
        success:function(response){
            //alert(response.mensaje);
            ModificarAlert();
            cargarDatos();
        },
      
      error:errorPeticion
        
    });
}
function SuccessAlert(){
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'registro',
        showConfirmButton: false,
        timer: 2000
      })
}

function ModificarAlert(){
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: '¿Estás seguro?',
        text: "¡No podrás revertir esto!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Sí, modificarlo!',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
          swalWithBootstrapButtons.fire(
            'Modificado!',
            'El registro ha sido modificado',
            'éxito'
          )
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelado',
          )
        }
      })
}


